const { response } = require('express');
const express = require('express');
const TypeArbreServices = require('../services/TypeArbre');
const router = express.Router();

const TypeArbreProvider = new TypeArbreServices();

router.get('/', async function (req, res, next) {
    res.status(201).json(await TypeArbreProvider.getAll());
});

router.get('/:id', async function (req, res, next) {
    const { id } = req.params;

    let code = 201;
    let response = {};

    const exists = await TypeArbreProvider.get(id);
    if (!exists) {
        code = 404;
        response = { code: 'not-found' };
    } else {
        response = await TypeArbreProvider.get(req.params.id);
    }

    res.status(code).json(response);

});

router.post('/', async function (req, res, next) {

    const { libelle } = req.body;

    let code = 201;
    let response = {};

    if (!libelle) {
        code = 400;
        response = { code: 'bad-request', error: 'Veuillez remplir les champs !' };
    } else {
        response = await TypeArbreProvider.create(libelle);
    }

    res.status(code).json(response);

});

router.put('/:id', async function (req, res, next) {

    const { libelle } = req.body;
    const { id } = req.params;

    let code = 201;
    let response = {};

    if (!libelle) {
        code = 400;
        response = { code: 'bad-request', error: 'Veuillez remplir les champs !' };
    } else {
        const exists = await TypeArbreProvider.get(id);
        if (!exists) {
            code = 404;
            response = { code: 'not-found' };
        } else {
            response = await TypeArbreProvider.update(libelle, id);
        }
    }

    res.status(code).json(response);
});


router.delete('/:id', async function (req, res, next) {

    const { id } = req.params;

    let code = 201;
    let response = {};

    const exists = await TypeArbreProvider.get(id);
    if (!exists) {
        code = 404;
        response = { code: 'not-found' };
    } else {
        response = await TypeArbreProvider.delete(id);
    }

    res.status(code).json(response);

});

module.exports = router;