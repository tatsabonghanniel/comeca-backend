const { response } = require('express');
const express = require('express');
const UsersServices = require('../services/Users');
const router = express.Router();

const UsersProvider = new UsersServices();

router.get('/', async function (req, res, next) {
    res.status(201).json(await UsersProvider.getAll());
});

router.get('/:id', async function (req, res, next) {

    const { id } = req.params;

    let code = 201;
    let response = {};

    const exists = await UsersProvider.get(id);
    if (!exists) {
        code = 404;
        response = { code: 'not-found' };
    } else {
        code = 201;
        response = await UsersProvider.get(req.params.id);
    }

    res.status(code).json(response);

});

router.post('/', async function (req, res, next) {
    const { nom, email, telephone, login, password } = req.body;

    let code = 201;
    let response = {};

    if (!nom || !email || !telephone || !login || !password) {
        code = 400;
        response = { code: 'bad-request', error: 'Veuillez remplir les champs !' };
    } else {
        response = await UsersProvider.register(nom, email, telephone, login, password);
    }

    res.status(code).json(response);
});

router.post('/login', async function (req, res, next) {
    const { login, password } = req.body;

    let code = 201;
    let response = {};

    code = 201;
    response = await UsersProvider.login(login, password);

    res.status(code).json(response);
});

router.put('/:id', async function (req, res, next) {

    const { nom, email, telephone, login, password } = req.body;
    const { id } = req.params;

    let code = 201;
    let response = {};

    if (!nom || !email || !telephone || !login || !password) {
        code = 400;
        response = { code: 'bad-request', error: 'Veuillez remplir les champs !' };
    } else {
        const exists = await UsersProvider.get(id);
        if (!exists) {
            code = 404;
            response = { code: 'not-found' };
        } else {
            response = await UsersProvider.update(nom, email, telephone, login, password, id);
        }
    }

    res.status(code).json(response);

});


// router.delete('/:id', async function (req, res, next) {

//     const { id } = req.params;

//     try {
//         res.status(201).json(await UsersProvider.delete(id));
//     } catch (err) {
//         console.error(`Error while getting arbres `, err.message);
//         next(err);
//     }
// });

module.exports = router;