const express = require('express');
const ArbresServices = require('../services/Arbres');
const router = express.Router();

const ArbresProvider = new ArbresServices();

router.get('/', async function (req, res, next) {
    res.status(201).json(await ArbresProvider.getAll());
});

router.get('/:id', async function (req, res, next) {

    const { id } = req.params;

    let code = 201;
    let response = {};

    const exists = await ArbresProvider.get(id);
    if (!exists) {
        code = 404;
        response = { code: 'not-found' };
    } else {
        response = await ArbresProvider.get(id);
    }

    res.status(code).json(response);
});

router.post('/', async function (req, res, next) {
    const { nom, nom_scientifique = "", description, image, idtype } = req.body;

    let code = 201;
    let response = {};

    if (!nom || !description || !image || !idtype) {
        code = 400;
        response = { code: 'bad-request', error: 'Veuillez remplir les champs !' };
    } else {
        response = await ArbresProvider.create(nom, nom_scientifique, description, image, idtype);
    }

    res.status(code).json(response);

});

router.put('/:id', async function (req, res, next) {

    const { nom, nom_scientifique = "", description, image, idtype } = req.body;
    const { id } = req.params;

    let code = 201;
    let response = {};

    if (!nom || !description || !image || !idtype) {
        code = 400;
        response = { code: 'bad-request', error: 'Veuillez remplir les champs !' };
    } else {
        const exists = await ArbresProvider.get(id);
        if (!exists) {
            code = 404;
            response = { code: 'not-found' };
        } else {
            response = await ArbresProvider.update(nom, nom_scientifique, description, image, idtype, id);
        }

    }

    res.status(code).json(response);
});


router.delete('/:id', async function (req, res, next) {

    const { id } = req.params;

    let code = 201;
    let response = {};

    const exists = await ArbresProvider.get(id);
    if (!exists) {
        code = 404;
        response = { code: 'not-found' };
    } else {
        response = await ArbresProvider.delete(id);
    }

    res.status(code).json(response);

});

module.exports = router;