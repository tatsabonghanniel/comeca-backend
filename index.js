const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = process.env.PORT || 5000;

const arbresRouter = require('./routes/Arbres');
const ExploitationRouter = require('./routes/Exploitations');
const TypesArbreRouter = require('./routes/TypeArbre');
const UsersRouter = require('./routes/Users');

app.use(bodyParser.json());
app.use(
    bodyParser.urlencoded({
        extended: true,
    })
);

app.get('/', (req, res) => {
    res.status(201).json({ 'message': 'ok' });
});

app.use('/arbres', arbresRouter);
app.use('/exploitations', ExploitationRouter);
app.use('/typesarbre', TypesArbreRouter);
app.use('/users', UsersRouter);

/* Error handler middleware */
app.use((err, req, res, next) => {
    const statusCode = err.statusCode || 500;
    console.error(err.message, err.stack);
    res.status(statusCode).json({ 'message': err.message });


    return;
});

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`)
});