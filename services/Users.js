const db = require('./db')

class UsersServices {

    async get(id) {

        const req = 'SELECT * FROM users WHERE id = ?';

        const result = await db.query(req, [id]);
        return result[0];
    }

    async getByEmail(email) {
        const req = 'SELECT * FROM users WHERE email = ?';

        return await db.query(req, [email]);
    }

    async getByEmailAndPassword(email, password) {
        const req = 'SELECT * FROM users WHERE email = ? AND password = ?';

        return await db.query(req, [email, password]);
    }

    async getAll() {
        const req = 'SELECT * FROM users';

        return await db.query(req, []);
    }

    async register(nom, email, telephone, login, password) {
        const req = `INSERT INTO users(nom, email, telephone, login, password, role) VALUES (?, ?, ?, ?, ?, 'paysan')`;
        const params = [nom, email, telephone, login, password];


        let already = (await this.getByEmail(email)).length;

        if (already) {
            return { success: false, error: 'already-exists' };
        }

        const res = await db.query(req, params);
        return { success: true, user: await this.get(res.insertId) };
    }

    async login(login, password) {
        const user = await this.getByEmailAndPassword(login, password);

        if (user.length) {
            return { success: true, user: user[0] };
        }

        return { success: false, error: 'no-match' };
    }

    async update(nom, email, telephone, login, password, id) {
        const req = `UPDATE users SET nom = ?, email = ?, telephone = ?, login = ?, password = ? WHERE id = ?`;
        const params = [nom, email, telephone, login, password, id];


        const user = await this.getByEmail(email);

        if (user[0].id != id) {
            return { success: false, error: 'already-exists' };
        }

        await db.query(req, params);
        return { success: true, user: { nom, email, telephone, login, password, id } };
    }
}

module.exports = UsersServices;