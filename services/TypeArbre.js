const db = require('./db')

class TypeArbreServices {

    async get(id) {

        const req = 'SELECT * FROM typearbre WHERE id = ?';

        const result = await db.query(req, [id]);
        return result[0];
    }

    async getAll() {
        const req = 'SELECT * FROM typearbre';
        return await db.query(req, []);
    }


    async create(libelle) {
        console.log('Libelle', libelle);
        const req = `INSERT INTO typearbre(libelle) VALUES (?)`;
        const params = [libelle];

        const res = await db.query(req, params);
        return { success: true, typearbre: await this.get(res.insertId) };
    }

    async delete(id) {
        const req = `DELETE FROM typearbre WHERE id = ?`;
        const params = [id];

        await db.query(req, params);
        return { success: true };
    }

    async update(libelle, id) {
        const req = `UPDATE typearbre SET libelle = ? WHERE id = ?`;
        const params = [libelle, id];

        await db.query(req, params);
        return { success: true, typearbre: { id, libelle } };
    }
}

module.exports = TypeArbreServices;