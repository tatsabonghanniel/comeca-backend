const db = require('./db');
const TypeArbreServices = require('./TypeArbre');

class ArbresServices {

    TypeArbreProvider;

    constructor() {
        this.TypeArbreProvider = new TypeArbreServices();
    }

    async get(id) {

        const req = 'SELECT * FROM arbres WHERE id = ?';

        const result = await db.query(req, [id]);

        const type = await this.TypeArbreProvider.get(result[0].idtype);
        return { ...result[0], type: type };
    }

    async getAll() {
        let result = [];
        const req = 'SELECT * FROM arbres';

        const arbres = await db.query(req, []);
        if (arbres) {
            for (const arbre of arbres) {
                const type = await this.TypeArbreProvider.get(arbre.idtype);
                result.push({ ...arbre, type: type });
            }
        }

        return result;
    }

    async create(nom, nom_scientifique, description, image, idtype) {
        const req = `INSERT INTO arbres(nom, nom_scientifique, description, image, idtype) VALUES (?, ?, ?, ?, ?)`;
        const params = [nom, nom_scientifique, description, image, idtype];

        const res = await db.query(req, params);
        return { success: true, arbre: await this.get(res.insertId) };
    }

    async delete(id) {
        const req = `DELETE FROM arbres WHERE id = ?`;
        const params = [id];

        await db.query(req, params);
        return { success: true };
    }

    async update(nom, nom_scientifique, description, image, idtype, id) {
        const req = `UPDATE arbres SET nom = ?, nom_scientifique = ? description = ?, image = ?, idtype = ? WHERE id = ?`;
        const params = [nom, nom_scientifique, description, image, idtype, id];

        await db.query(req, params);
        return { success: true, arbre: { id, nom, nom_scientifique, description, image, idtype } };
    }
}

module.exports = ArbresServices;