var mysql = require('mysql');
const config = require('../config');

async function query(sql, params) {

    let results = [];

    var con = mysql.createConnection(config.db);

    con.connect(function (err) {
        if (err) throw err;
        console.log("Connected!");
    });

    await new Promise((resolve, reject) => {
        con.query(sql, params,
            function (err, result) {
                if (err) {
                    reject(err);
                    throw err
                }
                results = result;
                resolve(results);
            });
    })

    con.end();


    return results;
}

module.exports = {
    query
}